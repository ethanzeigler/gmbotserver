package gmbotserver

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
	"strings"
)

func makeValidHook() *Hook {
	return &Hook{
		Name:     "Valid Hook",
		GroupIDs: []string{"fake_group_id"},
		Listener: func(hook *Hook, callback Callback) {
			hook.PostMessageSync(Message{
				BotID: "fake_bot_id",
				Text:  "All good!",
			}, 0)
		},
	}
}

func makeInvalidHookNoGroupId() *Hook {
	return &Hook{
		Name: "No Group IDs Hook",
		Listener: func(hook *Hook, callback Callback) {
			hook.PostMessageSync(Message{
				BotID: "fake_bot_id",
				Text:  "Huh? I have no group id!",
			}, 0)
		},
	}
}

func makeInvalidHookNoName() *Hook {
	return &Hook{
		GroupIDs: []string{"fake_group_id"},
		Listener: func(hook *Hook, callback Callback) {
			hook.PostMessageSync(Message{
				BotID: "fake_bot_id",
				Text:  "Huh? I have no hook id!",
			}, 0)
		},
	}
}

////////////////////////////////////////////////////

////////////////////////////////////////////////////

func TestRegisterhook(t *testing.T) {
	srv := NewGMServer()

	valid := makeValidHook()
	noName := makeInvalidHookNoName()
	noIds := makeInvalidHookNoGroupId()

	if !srv.Registerhook(valid) {
		t.Errorf("Valid hook wasn't accepted: %v", valid)
	}
	if srv.Registerhook(noName) {
		t.Errorf("Expected hook to be denied since no name was given: %v", noName)
	}
	if srv.Registerhook(noIds) {
		t.Errorf("Expected hook to be denied since no group ids were given: %v", noIds)
	}
}

func Testhook_RemoveHook(t *testing.T) {

}

func TestListenAndServe(t *testing.T) {
	srv := NewGMServer()
	srv.outputToBuffer = true

	validHook := makeValidHook()

	// Register hook
	srv.Registerhook(validHook)

	ts := httptest.NewServer(http.HandlerFunc(srv.handleRequest))
	defer ts.Close()

	request := httptest.NewRequest("POST", "http://example.com", bytes.NewBuffer([]byte("{\"attachments\":[],\"avatar_url\":\"https://i.groupme.com/750x750.jpeg.033de3766a1c414b99f9aad614937c7d\",\"created_at\":1536094557,\"group_id\":\"fake_group_id\",\"id\":\"153609455785477567\",\"name\":\"Neo Featherman\",\"sender_id\":\"31212732\",\"sender_type\":\"user\",\"source_guid\":\"2CE1796D-EA69-4609-8B1A-3058306EAAA6\",\"system\":false,\"text\":\"Do worker bees get compensated for accidents on the job?\",\"user_id\":\"31212732\"}")))
	writer := httptest.NewRecorder()
	srv.handleRequest(writer, request)
	time.Sleep(100 * time.Millisecond)

	if !strings.Contains(srv.restfulDebugBuffer.String(), "All good!") {
		t.Errorf("hook didn't reply with the all clear")
	}
}
