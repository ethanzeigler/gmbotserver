package gmbotserver

import (
	"strings"

	"github.com/sirupsen/logrus"
)

// Callback is
type Callback struct {
	// Time the message was created (unix time)
	CreatedAt uint64 `json:"created_at"`
	// The id of the group
	GroupID string `json:"group_id"`
	// UID of the message
	MessageID string `json:"id"`
	// UID of the sender
	SenderID string `json:"sender_id"`
	// Name of the sender (This is the nickname, and not static)
	SenderName string `json:"name"`
	// Type of the sender. I Have no idea what this means, I'll be honest.
	SenderType string `json:"user"`
	// I have no idea...
	SourceGUID string `json:"source_guid"`
	// Whether or not this is a system message.
	IsSystem bool `json:"system"`
	// The text of the message
	Text string `json:"text"`
	// The user id. I have no idea what that is
	UserID string `json:"user_id"`
	// The server GMServer this callback is from
	Server *GMServer
}

// hook to register to the groupme server.
// Register Hooks to a hook to add individual functions to the hook
// while maintaining modularity.
type Hook struct {
	// The IDs that the hook serves
	GroupIDs []string
	// name for debugging purposes
	Name string
	// Server, once attached
	server *GMServer
	// Callback listener
	Listener func(hook *Hook, callback Callback)
}

func (h Hook) Logger() *logrus.Logger {
	return h.server.Log
}

func (h *Hook) inputCallback(input Callback) {
	input.Text = strings.Trim(input.Text, " \n")
	h.server.Log.Debug("Invoking hook listener", logrus.Fields{
		"hook name":   h.Name,
		"sender": input.SenderName,
		"msg":    input.Text,
	})
	h.Listener(h, input)
}
