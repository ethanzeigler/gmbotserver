package gmbotserver

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"github.com/sirupsen/logrus"
)

type GMServer struct {
	// FIXME room for a race condition here that will need to be fixed in the future
	// Internal map from group ids to the linked hooks
	groupIDMap         map[string][]Hook
	hooks              []Hook
	outputToBuffer     bool
	restfulDebugBuffer bytes.Buffer

	// Conf stores configuration values from the local config.json file
	Config Configuration

	// Logger is the internal server logger made accessible for more complex requests
	// powered by logrus.
	Log *logrus.Logger
}

func (i *GMServer) init() {
	i.groupIDMap = make(map[string][]Hook)
	i.hooks = make([]Hook, 5)
	i.Log = logrus.New()
}

func NewGMServer() (server *GMServer) {
	server = new(GMServer)
	server.init()
	return
}

// Configuration stores data from the config.json file
type Configuration struct {
	// GroupMe API access token
	GmToken string `json:"gm_token"`
	// Port to bind the server to
	Binding string `json:"port"`
}


// Start opens the tcp port and begins listening for input.
// This is blocking for the runtime of the server
func (i *GMServer) Start() error {
	// default initialization failed?
	i.Log.WithFields(logrus.Fields{
		"binding": i.Config.Binding,
	}).Info("Starting server")

	if !i.checkConfiguration() {
		i.Log.Fatal("Server is improperly configured")
		return errors.New("server is improperly configured")
	}
	http.HandleFunc("/", i.handleRequest)
	err := http.ListenAndServe(i.Config.Binding, nil)
	i.Log.WithField("err", err).Fatal("web server failed")
	return err
}

func (i GMServer) checkConfiguration() bool {
	return len(i.Config.Binding) != 0
}

func (i *GMServer) handleRequest(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(200)
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		// not even worth logging
		return
	}

	var callback Callback
	err = json.Unmarshal(body, &callback)
	if err != nil {
		// log marshal failure
		i.Log.WithFields(logrus.Fields{
			"body":   body,
			"source": r.RemoteAddr,
		}).Warn("marshal failure")
		return
	}

	i.Log.WithField("callback", callback).Info("Received callback")

	if hooks, exists := i.groupIDMap[callback.GroupID]; exists {
		for _, hook := range hooks {
			i.Log.WithFields(logrus.Fields{
				"hook": hook.Name,
			}).Info("Alerted hook to callback")
			hook.inputCallback(callback)
		}
	} else {
		i.Log.WithFields(logrus.Fields{
			"group_id": callback.GroupID,
		}).Warn("Request from unserved group id")
		return
	}
}

// Registerhook registers a hook to serve the GM bot IDs and group IDs it defines
// returns true if hook was registered
func (i *GMServer) Registerhook(hook *Hook) bool {
	// Inspect hook for potential mistakes

	// check that the group has registered group IDs
	if len(hook.GroupIDs) < 1 {
		i.Log.WithFields(logrus.Fields{
			"hook": hook,
			"ids":  hook.GroupIDs,
		}).Warn("Hook has no group ids and will not register")
		return false
	}

	// check that the hook has registered hooks
	if len(hook.Name) < 1 {
		i.Log.WithFields(logrus.Fields{
			"hook": hook,
		}).Warn("Hook has no name and will not register")
		return false
	}

	// The hook is validated
	// We can now register it into the system
	hook.server = i

	// add to the list of all hooks
	i.hooks = append(i.hooks, *hook)
	// add into the map storing each hook to IDs it listens to
	// speed up lookup
	for _, groupID := range hook.GroupIDs {
		i.groupIDMap[groupID] = append(i.groupIDMap[groupID], *hook)
	}

	i.Log.WithFields(logrus.Fields{
		"name":   hook.Name,
		"groups": hook.GroupIDs,
	}).Info("Hook registered")
	return true
}
