package gmbotserver

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"
)

// Message represents a message to be sent to a groupme group chat.
// Use MakeMessage to initialize
type Message struct {
	// id of the bot to send the message
	BotID string `json:"bot_id"`
	// Text of the message. This is optional, but then must have a picture.
	Text string `json:"text,omitempty"`
	// Picture of the message. This is optional, but then must have text.
	// The provided URL must be from the groupme message API.
	Picture string `json:"picture_url,omitempty"`
}

// Posts the given message with the given delay to avoid the
// glitch where the response is before the message in groupme. #ThanksMS
// This is thread blocking.
func (i Hook) PostMessageSync(request Message, delay int) {
	i.postHelper(request, delay)
}

// Posts the given message with the given delay to avoid the
// glitch where the response is before the message in groupme. #ThanksMS
// This is asynchronous and not thread blocking.
func (i Hook) PostMessageAsync(request Message, delay int) {
	go i.postHelper(request, delay)
}

func (i *Hook) postHelper(request Message, delay int) {
	if delay > 0 {
		time.Sleep(time.Duration(int64(time.Millisecond) * int64(delay)))
	}
	jsonVal, err := json.Marshal(request)

	if err != nil {
		i.server.Log.WithField("err", err).Error("could not convert message request to json")
	}

	if !i.server.outputToBuffer {
		i.server.Log.WithField("json", string(jsonVal)).Info("posting message data to groupme")
		response, err := http.Post("https://api.groupme.com/v3/bots/post",
			"application/json", bytes.NewBuffer(jsonVal))

		if err != nil {
			i.server.Log.WithField("err", err.Error()).Error("send message request unsuccessful")
		} else {
			body, _ := ioutil.ReadAll(response.Body)
			i.server.Log.WithField("response", string(body)).Info("send message request successful")
		}
	} else {
		i.server.Log.WithField("json", string(jsonVal)).Info("Writing RESTful request to buffer")
		i.server.restfulDebugBuffer.Write(jsonVal)
	}
}

func RegisterImage(file string) (string, error) {
	//extSplit := strings.Split(file, ".")
	//ext := extSplit[len(extSplit)-1]
	//var err error
	//// check file extension
	//if strings.Compare(ext, "jpg") == 0 ||
	//	strings.Compare(ext, "jpeg") == 0 ||
	//	strings.Compare(ext, "gif") == 0 ||
	//	strings.Compare(ext, "png") == 0 {
	//
	//	cmd := exec.Command("curl", "https://image.groupme.com/pictures", "-X", "POST", "-H", "X-Access-Token: "+env.GmToken, "-H", "Content-Type: image/"+ext, "--data-binary", "@"+file)
	//	err = cmd.Run()
	//	if err != nil {
	//		return "", err
	//	} else {
	//		val, err := cmd.Output()
	//		Logger.WithField("response", val).Info("gm image service request successful")
	//		return string(val), err
	//	}
	//} else {
	//	err = errors.New("invalid file extension")
	//	Logger.WithField("err", err.Error()).Error("could not register image to gm image service")
	//	return "", err
	//}
	return "", nil
	// TODO implement
}
